let express = require('express');
let app = express();
let mysql = require('mysql2/promise');
require('dotenv').config();

let connection;

async function databaseCon(){
    connection = await mysql.createConnection({
        user: process.env.USSER,
        host: process.env.HOST,
        password: process.env.PASSWORD,
        database: process.env.DATABASE
    })
    
};

databaseCon();

// POST
app.post('/contact',async(req,res)=>{
    try {
        const [results,data] = await connection.query('INSERT INTO users(first_name, last_name, phone_number, work_number) VALUES (?,?,?,?)',
        [req.body.first_name, req.body.last_name, req.body.phone_number, req.body.work_number])
        res.json(results)

    }
    catch(err){
        console.log(err)
    }
});


// GET
app.get('/contact:id', async (req,res)=>{
    try{
        const[rows, fiedls] = await connection.execute(`SELECT * FROM users WHERE id=${req.params.id}`)
        res.json(rows)

    }
    catch(err){
        console.log(err)
    }
});

// PUT
app.put('/contact/:id', async(req, res)=>{
    try{
        const[rows, fields] = await connection.execute(`SELECT * FROM users WHERE id=${req.params.id}`);
        const[results, data] = await connection.query(`UPDATE users SET first_name=?, last_name=?, phone_number=?,work_number=? WHERE id=${req.params.id}`,
        [req.body.first_name, req.body.last_name, req.body.phone_number, req.body.work_number]);
        res.json(results);
    }
    catch(err){
        console.log(err);
    }
});


// DELETE
app.delete('contact/:id', async(req, res)=>{
    try{
        const[rows, fields] = await connection.execute(`SELECT * FROM users WEHRE id =${req.params.id}`);
        const[results,data] = await connection.execute(`DELETE FROM users WHERE id=${req.params.id}`);
        res.json({message:'contact deleted succesfully'})
    }
    catch(err){
        console.log(err);
    }
})

// GET all contacts
app.get ('/contact', async(req,res)=>{
    try{
        const[rows, fiedls] = connection.execute(`SELECT * FROM users`);
        res.json(rows);
        // dlelte
        console.log(rows);
    }
    catch(err){
        console.log(err);
    }
});

let port = process.env.PORT;

app.listen(port,()=>{'listening...'})